<?php

namespace Tests\Feature\Controller;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function testAllEvents(): void
    {
        $response = $this->get('/api/v1/events', [
            'Authorization' => 'Bearer ' . config('auth-bearer.key')
        ]);

        $response->assertStatus(200);
    }
}
