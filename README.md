# API TEST Laravel

## Swagger/OpenAPI - документация
[http://localhost:8020/swagger]

##  Запуск Local
1. Создать .env файл
```
cp .env.example .env
```
2. Установить ключ авторизации
```
AUTH_KEY=pm.....w
```
3. Запуск docker
```
./docker.sh up -d --build
```
4. Установка composer backend
```
./docker.sh web composer install
```
5. Сгенерировать ключ приложения
```
./docker.sh web php artisan key:generate
```

## Запуск тестов
```
./docker.sh web php artisan test
```

## Собрать swagger
```
./docker.sh web php artisan l5-swagger:generate
```

## Команды docker
```
./docker.sh web php artisan ***
```
