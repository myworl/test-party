#!/bin/bash
MAIN=docker-compose.yml
CONNECT="-f $MAIN"

function ComposeCommand {
    docker-compose $CONNECT $@
}

function ComposeCommandExec {
     docker-compose exec -T --user $(id -u):$(id -g) $@
}

function run() {
    COMMAND=$1
    case "$COMMAND" in
        down|up|stop|restart|build|pull|logs|rm|run)
            ComposeCommand $@
        ;;
        web)
            shift
            echo 'Exec PHP script '
            ComposeCommandExec web $@
        ;;
        exec)
            shift
            ComposeCommandExec $@
        ;;
    esac
}

run $@
