<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventReserveRequest;
use App\Repositories\ApiTestRepository;
use App\Services\EventService;
use Illuminate\Http\JsonResponse;

class EventController extends Controller
{
    public function __construct(
        public ApiTestRepository $apiTestRepository,
    ){}

    /**
     * @OA\Get (
     *     path="/api/v1/events",
     *     operationId="allEvents",
     *     tags={"Events"},
     *     summary="Списка мероприятий",
     *     security={{ "api_key": {} }},
     *     @OA\Response(response="200", description="OK",
     *        @OA\JsonContent(
     *              @OA\Property(property="data", type="array", @OA\Items(
     *                  @OA\Property( property="id", type="integer", example="1"),
     *                  @OA\Property( property="name", type="strung", example="Show #1"),
     *            )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка сервера",
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *     ),
     * )
     */
    public function allEvents(): JsonResponse
    {
        try {
            return $this->sendResponse(['data' => $this->apiTestRepository->allEvents()]);
        } catch (\Exception $e) {
            return $this->sendResponse([
                'data' => NULL,
                'errors' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @OA\Get (
     *     path="/api/v1/event/{id}",
     *     operationId="showEvent",
     *     tags={"Events"},
     *     summary="Список событий мероприятия",
     *     security={{ "api_key": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="showId",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="OK",
     *        @OA\JsonContent(
     *              @OA\Property(property="data", type="array", @OA\Items(
     *                  @OA\Property( property="id", type="integer", example="1"),
     *                  @OA\Property( property="showId", type="integer", example="1"),
     *                  @OA\Property( property="date", type="strung", example="2023-10-09 10:38:22"),
     *            )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка сервера",
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *     ),
     * )
     */
    public function showEvent($id): JsonResponse
    {
        try {
            return $this->sendResponse(['data' => $this->apiTestRepository->showsEvents($id)]);
        } catch (\Exception $e) {
            return $this->sendResponse([
                'data' => NULL,
                'errors' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @OA\Get (
     *     path="/api/v1/event/{id}/place",
     *     operationId="showEventPlace",
     *     tags={"Events"},
     *     summary="Список мест события",
     *     security={{ "api_key": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="eventId",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="OK",
     *        @OA\JsonContent(
     *              @OA\Property(property="data", type="array", @OA\Items(
     *                  @OA\Property( property="id", type="integer", example="1"),
     *                  @OA\Property( property="x", type="integer", example="0"),
     *                  @OA\Property( property="y", type="integer", example="0"),
     *                  @OA\Property( property="width", type="integer", example="20"),
     *                  @OA\Property( property="height", type="integer", example="20"),
     *                  @OA\Property( property="is_available", type="bool", example="true"),
     *            )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка сервера",
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *     ),
     * )
     */
    public function showEventPlace($id): JsonResponse
    {
        try {
            return $this->sendResponse(['data' => $this->apiTestRepository->showsEventsPlace($id)]);
        } catch (\Exception $e) {
            return $this->sendResponse([
                'data' => NULL,
                'errors' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * @OA\Post (
     *     path="/api/v1/event/{id}/reserve",
     *     operationId="eventReserve",
     *     tags={"Events"},
     *     summary="Забронировать места события",
     *     security={{ "api_key": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="eventId",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *           required=true,
     *         @OA\JsonContent(
     *              @OA\Property(property="name", type="string", format="text", example="Alex", description="Имя клиента"),
     *              @OA\Property(property="places", type="integer", format="text", example="1", description="Номер метста мероприятия"),
     *            ),
     *     ),
     *     @OA\Response(response="200", description="OK",
     *        @OA\JsonContent(
     *              @OA\Property(property="data", type="array", @OA\Items(
     *                  @OA\Property( property="success", type="bool", example="true"),
     *                  @OA\Property( property="reservation_id", type="string", example="5d519fe58e3cf"),
     *            )),
     *         ),
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Ошибка сервера",
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *     ),
     * )
     */
    public function eventReserve($id, EventReserveRequest $request, EventService $eventService): JsonResponse
    {
        try {
           return $this->sendResponse([
                'data' => $eventService->checkedEventReserve($id, $request->post('name'), $request->post('places'))
            ]);
        } catch (\Exception $e) {
            return $this->sendResponse([
                'data' => NULL,
                'errors' => $e->getMessage()
            ], $e->getCode());
        }
    }
}
