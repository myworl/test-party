<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="api_test",
 *      description="Framework Laravel 10"
 * ),
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     securityScheme="api_key",
 *     name="Authorization",
 *     description="Токен в формате Authorization: Bearer *****",
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function sendResponse(array $data = [], int $statusCode = Response::HTTP_OK, array $headers = ['Content-Type' => 'application/json'], $options = 0): JsonResponse
    {
        return response()->json($data, $statusCode, $headers, $options);
    }
}
