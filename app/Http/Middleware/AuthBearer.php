<?php

namespace App\Http\Middleware;


use Illuminate\Http\Request;

class AuthBearer
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next): mixed
    {
        $token = 'Bearer ' . config('auth-bearer.key');
        if ($token !== $request->header('Authorization')) {
            return response()->json(['data' => 'Unauthorized'], 401);
        }
        return $next($request);
    }
}
