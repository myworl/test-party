<?php

namespace App\Services;

use App\Repositories\ApiTestRepository;
use Exception;

class EventService
{
    public function __construct(
        private readonly ApiTestRepository $apiTestRepository,
    ){}

    /**
     * Проверка места (свободно/занято)
     * @param int $id
     * @param string $name
     * @param int $place
     * @return mixed
     * @throws Exception
     */
    public function checkedEventReserve(int $id, string $name, int $place): mixed
    {
        // Поиск мест события
        $eventPlace = $this->apiTestRepository->showsEventsPlace($id);

        $is_available = array_filter($eventPlace, function ($v) use ($place) {
            return $v['id'] == $place && $v['is_available'] == true;
        }, ARRAY_FILTER_USE_BOTH);

        if (!$is_available) {
            throw new Exception('Выбранное место занято!', 200);
        }

        // Бронируем
        return $this->apiTestRepository->eventsReserve($id, $name, $place);
    }
}
