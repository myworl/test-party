<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Http;
use Mockery\Exception;
use function Laravel\Prompts\error;


class ApiTestRepository
{
    const URL_TEST_API = 'https://leadbook.ru/test-task-api';


    /**
     * Список мероприятий
     * @return mixed
     * @throws \Exception
     */
    public function allEvents(): mixed
    {
        $response = Http::get(self::URL_TEST_API . '/shows');
        $data = $response->json();

        if ($response->status() === 200 && isset($data['response'])) {
            return $data['response'];
        }

        return throw new \Exception('Request error!', 400);
    }

    /**
     * Список событий мероприятия
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function showsEvents(int $id): mixed
    {
        $response = Http::get(self::URL_TEST_API . '/shows/' . $id . '/events');
        $data = $response->json();

        if ($response->status() === 200 && isset($data['response'])) {
            if(empty($data['response'])) {
                return throw new \Exception('Мероприятия с id:' . $id . ' не существует!');
            }
            return $data['response'];
        }
        return throw new \Exception('Request error!', 400);
    }

    /**
     * Список событий мероприятия
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function showsEventsPlace(int $id): mixed
    {
        $response = Http::get(self::URL_TEST_API . '/events/' . $id . '/places');
        $data = $response->json();

        if ($response->status() === 200 && isset($data['response'])) {
            if(empty($data['response'])) {
                return throw new \Exception('Мероприятия с id:' . $id . ' не существует!');
            }
            return $data['response'];
        }
        return throw new \Exception('Request error!', 400);
    }

    /**
     * Забронировать места события
     * @param int $id
     * @param string $name
     * @param int $place
     * @return mixed
     * @throws \Exception
     */
    public function eventsReserve(int $id, string $name, int $place): mixed
    {

        $response = Http::asForm()->post(
            self::URL_TEST_API . '/events/' . $id . '/reserve', [
                'name' => $name,
                'places' => $place
            ]
        );
        $data = $response->json();

        if ($response->status() === 200 && isset($data['response'])) {

            if(isset($data['response']['success']) && $data['response']['success'] !== true) {
                return throw new \Exception('Не удалось забронировать места:'. $place);
            }

            return $data['response'];
        }

        if(isset($data['error'])) {
            return throw new \Exception($data['error'], 200);
        }

        return throw new \Exception('Request error!', 400);
    }
}
