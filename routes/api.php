<?php

use App\Http\Controllers\API\EventController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->middleware('auth.bearer')->group(function () {
    Route::get('/events', [EventController::class, 'allEvents']);
    Route::get('/event/{id}', [EventController::class, 'showEvent'])->where(['id' => '[0-9]+']);
    Route::get('/event/{id}/place', [EventController::class, 'showEventPlace'])->where(['id' => '[0-9]+']);
    Route::post('/event/{id}/reserve', [EventController::class, 'eventReserve'])->where(['id' => '[0-9]+']);
});
